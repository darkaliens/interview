<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use ZipArchive;
use App\Imports\UsersImport;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Concerns\ToModel;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('welcome');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

$za = new ZipArchive();

$res=$za->open($request->file('emp'));

for( $i = 0; $i < $za->numFiles; $i++ ){
    $stat = $za->statIndex( $i );
    $filenames[]=$stat['name'];

}
        if ($res=== TRUE) {
            $za->extractTo('uploads');
            $za->close();

        }
       // $rows = Excel::toArray('uploads\sample.xls')->get();
        foreach ($filenames as $row)
        {
            $type=explode('.',$row);
            if($type[1]=='xls')
            {
             Excel::import(new UsersImport,'./uploads/'.$row);
            }
        }
        foreach ($filenames as $row)
        {

            $type=explode('.',$row);
            if($type[1]!='xls')
            {
              Employee::where('name',$type[0])->update(['profile_picture'=>'./uploads/'.$row]);
            }
        }
        //;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
