<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    //

    public function employee()
    {
        return $this->belongsToMany(Employee::class);
    }
}
