<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //

/*$table->bigIncrements('id');
$table->string('email');
$table->string('phone',12);
$table->string('gender',1);
$table->integer('company_id');
$table->date('date_of_join');
$table->string('profile_picture');*/
protected $fillable=['email','phone','gender','company_id','date_of_join','profile_picture','name'];

public function course()
{
    return $this->belongsToMany(Course::class);
}
}
