<?php

namespace App\Imports;

use App\Course;
use App\Employee;
use Maatwebsite\Excel\Concerns\ToModel;

class UsersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        if(isset($row[1])&&$row[1]!='email')

        {
if(Course::where('name', $row[6])->first()!=null)//check course exist
{
    $emp = Employee::create([

        'name'     => $row[0],
        'email'    => $row[1],
        'phone'=>$row[2],
        'gender'=>$row[3],
        'company_id'=>$row[4],
        'date_of_join'=>date('Y-m-d', strtotime(strtr($row[5], '/', '-'))),

        //
    ]);
    //  dd($emp);


    return $emp
        ->course()
        ->attach(Course::where('name', $row[6])->first());
}

        }
    }
}
