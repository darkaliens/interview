<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
/*        name
email
phone
gender
company_id
date_of_join
profile_picture
Created_DateTime
Modified_DateTime*/
        Schema::create('employees', function (Blueprint $table) {
            $table->string('name');
            $table->bigIncrements('id');
            $table->string('email');
            $table->string('phone',12);
            $table->string('gender',1);
            $table->integer('company_id');
            $table->date('date_of_join');
            $table->string('profile_picture')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
